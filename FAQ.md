# Developer FAQ

> You can find the official CTFd documentation [here](https://docs.ctfd.io/).

## 1. Where do I start as a software developer?

As a software developer you will primarily work on 3 things:

- **Plugins**:  CTFd allows for custom Python plugins that can interact with the CTFd core system. Plugins are located in [this directory](CTFd/plugins/).
- **Themes**: CTFd has stored everything related to the frontend theme of the website in the [themes directory](CTFd/themes/).
- **Configuration**: CTFd uses a [configuration file](CTFd/config.ini) to store global constants that can be used througout the codebase.

Try to develop the platform in the ways listed above and edit the core files of CTFd **only as a last case scenario**. You can practically change/overwrite any CTFd functionality by writing custom plugins.

> You can find all Python dependencies in the `requirements.txt` file.

## 2. Where do I start as a cloud developer?

Cloud developers usually work on infrastructure, security, pipelines, et cetera. Currently, there is nothing in this repository that is interesting for cloud developers.

## 3. How do I write a plugin?

The [official CTFd documentation](https://docs.ctfd.io/) explains how you can develop plugins for CTFd. However, the JCR created a [tutorial](CTFd/plugins/README.md) for software developers who are new to this project. You can use this tutorial to create your own plugin.

## 4. How do I write plugin documentation?

After or during the development of a plugin, you must write documentation. This is necessary so other software developers can easily learn about the plugin and get a fast overview of the codebase. Look at the documentation of existing plugins for an example.

### 4.1 Requirements

- All Python code contains comments that describe and/or explain the code.
- Every plugin has a `README.md` file in the plugin folder root.
- If you need multiple Markdown files, put these files in a `readme/` folder.

### 4.2 README contents

- Brief explanation of the plugin
- `1. Features` section
- `2. Happy flow` section
- `3. Technical Documentation` section
- `4. Abbreviations` section
- `5. Used Technologies/Concepts` section

## 5. How do I clear the database?

During development it is common to test functionality for which you need to clear the database first. There are 2 ways to do this:

- Remove the `CTFd/ctfd.db` file and the `.data` folder.
- Execute the `scripts/bat/wipe_database.bat` file (Windows)
- Execute the `scripts/sh/wipe_database.sh` file (Linux / MacOS)

## 6. How do I write commit messages?

The JCR uses a lightweight commit message convention based on [conventional commits](https://www.conventionalcommits.org/). A commit message convention improves the readability and traceability of the development history in Gitlab. All commit messages in this repository should follow this message convention.

### 6.1 Commit message template

```text
<type>: <description>
```

### 6.2 Types to choose from

- **build**: changes that affect the build system
- **cicd**: changes to the CI/CD configuration
- **clean**: the removal of files or deprecated features/content
- **docs**: changes to documentation
- **feat**: a new feature
- **fix**: a bug fix
- **perf**: a change that improves performance
- **refac**: a change that refactors existing features
- **style**: a change to code style/formatting
- **test**: a new/changed test

## 7. How do I keep everything up-to-date?

> Current CTFd version: 3.5.0, released on 2023-01-23

Get notified for a new release on GitHub by watching the repository.

### 7.1 CTFd update guide

#### 7.1.1 Preparation

- Download the latest CTFd version source code from [GitHub](https://github.com/CTFd/CTFd/releases)
- Create and checkout a new branch called `update` in the `ctf-platform` repository.

#### 7.1.2 Update

- Replace the `CTFd/` directory with the **new** version
- Replace the `migrations/` directory with the **new** version
- Replace the `scripts/` directory with the **new** version
- Replace the `requirements.txt` file with the **new** version

#### 7.1.2.1 changing config.ini
- Change `[email]` to `[mail]`
- From the previous config.ini file, add the custom JCR lines from `[setup]` and `[email]` to the new config.ini

#### 7.1.2.2 changing config.py
- Between line 165 and 190, change all mentions of `[email]` to `[mail]`

As an example: <br>
Change: 
```python
MAIL_SERVER: str = empty_str_cast(config_ini["email"]["MAIL_SERVER"])
```
To:
```python
MAIL_SERVER: str = empty_str_cast(config_ini["mail"]["MAIL_SERVER"])
```

**NOTE:** The location might change with different versions of CTFd

#### 7.1.3 Dependencies

- Add the lines from the `8.2 Python dependencies added by the JCR` section to the `requirements.txt` file
- Remove any comments or empty lines from the `requirements.txt` file (optional)
- Alphabetize all lines in the `requirements.txt` file (optional)

#### 7.1.4 Plugins

- Add all directories from the `8.3 Plugins added by the JCR` section to the `CTFd/plugins/` directory

#### 7.1.5 Themes

- Replace the `CTFd/themes/core/static/img/logo.png` file with the **old** version
- Replace the `CTFd/themes/core/static/img/favicon.ico` file with the **old** version

#### 7.1.6 Configurations

- Add the variables from the `8.4 Configurations added by the JCR` section to the `CTFD/config.py` file
- Add the variables from the `8.4 Configurations added by the JCR` section to the `CTFD/config.ini` file

#### 7.1.7 Finalizing

- Test if everything is still working properly
- Merge the `update` branch with the `master` branch
- Replace the version number in this guide

### 7.2 Dependency update guide

If you do not want to update to a newer CTFd version, you can still update the dependencies (`requirements.txt`) to their newest versions. This contains security updates, optimizations, improvements, etc. The newest dependency versions can be found at [Python Package Index](https://pypi.org/). However, there is a much faster way to update dependency versions: a Python package called [pip-upgrader](https://pypi.org/project/pip-upgrader/).
To update all version *numbers* in `requirements.txt`, follow these steps:

```shell
> pip install pip-upgrader
> cd path/to/repository/
> pip-upgrade --skip-package-installation
```

**WARNING**: be careful with updating dependencies, as it can break the repository!

## 8. How do I know if something is JCR or CTFd?

### 8.1 CTFd files changed by the JCR

- `.dockerignore`
- `.gitignore`
- `Dockerfile`
- `manage.py`
- `package.json`
- `README.md`
- `requirements.txt`
- `yarn.lock`
- `CTFd/config.ini`
- `CTFd/config.py`
- `CTFd/themes/admin/templates/base.html`
- `CTFd/themes/core/templates/base.html`
- `CTFd/themes/core/static/img/logo.png`
- `CTFd/themes/core/static/img/favicon.ico`

### 8.2 Python dependencies added by the JCR

- `authlib==0.15.5`
- `kubernetes==17.17.0`

### 8.3 Plugins added by the JCR

- `CTFd/plugins/container_challenges/`
- `CTFd/plugins/custom_auth/`
- `CTFd/plugins/initial_setup/`

### 8.4 Configurations added by the JCR

- SETUP_EVENT_NAME
- SETUP_EVENT_MODE
- SETUP_EVENT_ADMIN
- SETUP_ACCOUNT_TYPE
- SETUP_EMAIL_ENABLED
- EMAIL_SERVER
- EMAIL_PORT
- EMAIL_PROTOCOL
- EMAIL_ADDRESS
- EMAIL_USERNAME
- EMAIL_PASSWORD
- SURF_META
- SURF_SCHEME
- SURF_CLIENT_ID
- SURF_CLIENT_SECRET
